<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prueba_Version;
class PruebaTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horariosprueba = Prueba_Version::all();
        return view ("TestHorarios.index", compact ("horariosprueba"));
       // return $horariosprueba;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('TestHorarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Prueba_Version::create($request->all());
        return redirect()->route('horariosprueba.index')->with('success','Horario creado exitosamente!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Prueba_Version $horariosprueba)
    {
        return view('TestHorarios.edit', compact ("horariosprueba"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $horariosprueba = Prueba_Version :: find ($id);
        $horariosprueba -> update ($request -> all());
        return redirect () -> route ("horariosprueba.index") -> with ("success","Horario modificado con exito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horariosprueba = Prueba_Version :: find ($id);
        $horariosprueba -> delete ();
        return redirect () -> route ("horariosprueba.index") -> with ("success","Horario eliminado con exito");
    }
}
