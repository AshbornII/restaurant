<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menus;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menutable = Menus::all();
        return view ("Menu.index", compact ("menutable"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Menus::create($request->all());
        return redirect()->route('menutable.index')->with('success','Su reserva de menu fue creada de forma exitosa!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menus $menutable)
    {
        return view('Menu.edit', compact ("menutable"));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $menutable = Menus :: find ($id);
            $menutable -> update ($request -> all());
            return redirect () -> route ("menutable.index") -> with ("success","Menu modificado con exito");
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menutable = Menus :: find ($id);
        $menutable -> delete ();
        return redirect () -> route ("menutable.index") -> with ("success","Menu eliminado con exito");
    }
}
