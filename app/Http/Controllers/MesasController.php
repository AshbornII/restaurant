<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mesas;

class MesasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesastable = Mesas::all();
        return view ("WMesa.index", compact ("mesastable"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('WMesa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Mesas::create($request->all());
        return redirect()->route('mesastable.index')->with('success','Su reserva de mesa fue creada de forma exitosa!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mesas $mesastable)
    {
        return view('WMesa.edit', compact ("mesastable"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mesastable = Mesas :: find ($id);
        $mesastable -> update ($request -> all());
        return redirect () -> route ("mesastable.index") -> with ("success","Mesa modificada con exito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesastable = Mesas :: find ($id);
        $mesastable -> delete ();
        return redirect () -> route ("mesastable.index") -> with ("success","Mesa eliminado con exito");
    }
}
