<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prueba_Version extends Model
{
    use HasFactory;
    protected $table = "horario";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id",
        "hora_apertura",
        "hora_cierre",
        "dia_Atencion",
    ];
}
