<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use HasFactory;
    protected $table = "estado_mesa";
    protected $primarykey = "id ";
    public $timestamps = false;
    protected $fillable = [
        "id",
        "numero_mesa ",
        "estado_ocupada",
    ];
}
