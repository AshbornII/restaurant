<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    use HasFactory;
    protected $table = "pedidos";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [
        "id",
        "menu_id_menu  ",
        "clientes_id_cliente ",
        "mesas_id_mesa ",
        "tipo_de_pedido",
    ];
}
