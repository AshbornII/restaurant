<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mesas extends Model
{
    use HasFactory;
    protected $table = "mesas";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id",
        "numero_mesa",
        "color_mesa",
    ];
}
