<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    use HasFactory;
    protected $table = "clientes";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id",
        "nombre_cliente",
        "telefono_cliente",
        "direccion_cliente",
        "ciudad_cliente",
    ];
}
