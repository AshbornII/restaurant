<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    use HasFactory;
    protected $table = "menu";
    protected $primarykey = "id ";
    public $timestamps = false;
    protected $fillable = [
        "id",
        "menu ",
        "precio_menu",
        "descripcion_menu",
    ];
}
