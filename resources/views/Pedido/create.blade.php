@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Cliente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('Pedido.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('Pedido.store') }}" method="POST">
            @csrf

            <div class="row">
                
                <div>
                    <div class="form-group">
                        <strong>Nombre del Menu:</strong>
                        <input type="varchar" name="menu_id_menu " class="form-control"
                            placeholder="Ingrese el nombre del menu">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Nombre del Cliente que tomo el menu:</strong>
                        <input type="varchar" name="clientes_id_cliente " class="form-control"
                            placeholder="Ingrese el nombre del cliente">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Mesa:</strong>
                        <input type="varchar" name="mesas_id_mesa " class="form-control"
                            placeholder="Ingrese la mesa del cliente">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Ingresar el tipo de Pedido:</strong>
                        <input type="varchar" name="tipo_de_pedido" class="form-control"
                            placeholder="Ingrese la mesa del cliente">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection