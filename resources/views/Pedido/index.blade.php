@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE PEDIDOS</h1>
        <div>
            <a class="btn btn-success" href="{{ route('Pedido.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>ID Menu</th>
                <th>ID Cliente</th>
                <th>ID Mesa</th>
                <th>Tipo de Pedido</th>
                <th>Acciones</th>
            </tr>
            @foreach ($Pedido as $pedidoss)
                <tr>
                    <td>{{ $pedidoss->id_pedido   }}</td>
                    <td>{{ $pedidoss->menu_id_menu  }}</td>
                    <td>{{ $pedidoss->clientes_id_cliente }}</td>
                    <td>{{ $pedidoss->mesas_id_mesa }}</td>
                    <td>{{ $pedidoss->tipo_de_pedido }}</td>
                    <td><i class="fa-solid fa-pen-to-square"></i><i class="fa-solid fa-trash"></i></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
   