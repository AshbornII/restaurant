@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Horario</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('mesastable.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('mesastable.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Numero de mesa:</strong>
                        <input type="varchar" name="numero_mesa" class="form-control"
                            placeholder="Ingrese el numero de mesa">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Color de mesa:</strong>
                        <input type="varchar" name="color_mesa" class="form-control"
                            placeholder="Ingrese el color de la mesa deseada">
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection