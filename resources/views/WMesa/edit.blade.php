@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar la Mesa</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('mesastable.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('mesastable.update',$mesastable->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Ingresar el Numero de Mesa:</strong>
                    <input type="varchar" name="numero_mesa" class="form-control" value="{{ $mesastable->numero_mesa }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Ingresar el color de Mesa:</strong>
                    <input type="varchar" name="color_mesa" class="form-control" value="{{ $mesastable->color_mesa }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
