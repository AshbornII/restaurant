@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE HORARIOS DE ATENCIÓN</h1>
        <div>
            <a class="btn btn-success" href="{{ route('mesastable.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Numero de mesa</th>
                <th>Color de mesa</th>
                <th>Acciones</th>
            </tr>
            @foreach ($mesastable as $mesa)
                <tr>
                    <td>{{ $mesa->id}}</td>
                    <td>{{ $mesa->numero_mesa }}</td>
                    <td>{{ $mesa->color_mesa }}</td>
                    <td><a class="btn btn-primary" href="{{ route('mesastable.edit',$mesa) }}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    <form action="{{ route('mesastable.destroy',$mesa->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
