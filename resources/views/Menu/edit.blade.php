@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar el Menu</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('menutable.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('menutable.update',$menutable->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre del menu:</strong>
                    <input type="text" name="menu" class="form-control" value="{{ $menutable->tipo_menu }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Precio del Menu:</strong>
                    <input type="varchar" name="precio_menu" class="form-control" value="{{ $menutable->precio_menu }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Descripcion del Menu:</strong>
                    <input type="varchar" name="descripcion_menu" class="form-control" value="{{ $menutable->descripcion_menu }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
