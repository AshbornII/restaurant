@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo MENU</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('menutable.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('menutable.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre del Menu:</strong>
                        <input type="text" name="menu" class="form-control"
                            placeholder="Ingrese el nombre del menu">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Precio del Menu:</strong>
                        <input type="varchar" name="precio_menu" class="form-control"
                            placeholder="Seleccionar el tamaño del menu con su respectivo precio">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Descripcion del menu:</strong>
                        <input type="varchar" name="descripcion_menu" class="form-control"
                            placeholder="Ingrese la descripcion del menu">
                    </div>
                </div>

            

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection