@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE MENU</h1>
        <div>
            <a class="btn btn-success" href="{{ route('menutable.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre del Menu</th>
                <th>Precio del Menu</th>
                <th>Descripccion del menu</th>
                <th>Acciones</th>
            </tr>
            @foreach ($menutable as $menu)
                <tr>
                    <td>{{ $menu->id   }}</td>
                    <td>{{ $menu->menu }}</td>
                    <td>{{ $menu->precio_menu}}</td>
                    <td>{{ $menu->descripcion_menu }}</td>
                    <td><a class="btn btn-primary" href="{{ route('menutable.edit',$menu) }}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    <form action="{{ route('menutable.destroy',$menu->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
