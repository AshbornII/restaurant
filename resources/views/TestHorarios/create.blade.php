@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Horario</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('horariosprueba.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('horariosprueba.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Día de Reserva:</strong>
                        <input type="date" name="dia_Atencion" class="form-control"
                            placeholder="Ingrese el día de reserva">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Inicio de Reserva:</strong>
                        <input type="time" name="hora_apertura" class="form-control"
                            placeholder="Ingrese la hora de la reserva">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Fin de Reserva:</strong>
                        <input type="time" name="hora_cierre" class="form-control"
                            placeholder="Ingrese la hora fin reserva">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection