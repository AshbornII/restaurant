@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Horario de Atención</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('horariosprueba.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('horariosprueba.update',$horariosprueba->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Día Atención:</strong>
                    <input type="date" name="dia_Atencion" class="form-control" value="{{ $horariosprueba->dia_Atencion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Inicio Atención:</strong>
                    <input type="time" name="hora_apertura" class="form-control" value="{{ $horariosprueba->hora_apertura }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Fin Atención:</strong>
                    <input type="time" name="hora_cierre" class="form-control" value="{{ $horariosprueba->hora_cierre }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
