@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Estado de la Mesa</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('estadotable.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('estadotable.update',$estadotable->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Numero de la Mesa:</strong>
                    <input type="text" name="numero_mesa" class="form-control" value="{{ $estadotable->numero_mesa }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Estado de la Mesa:</strong>
                    <input type="varchar" name="estado_ocupada" class="form-control" value="{{ $estadotable->estado_ocupada }}">
                </div>
            </div>
           
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
