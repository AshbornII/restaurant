@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE ESTADOS DE MESAS</h1>
        <div>
            <a class="btn btn-success" href="{{ route('estadotable.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Numero de mesa</th>
                <th>Estado de la mesa</th>
                <th>Acciones</th>
            </tr>
            @foreach ($estadotable as $estado)
                <tr>
                    <td>{{ $estado->id  }}</td>
                    <td>{{ $estado->numero_mesa }}</td>
                    <td>{{ $estado->estado_ocupada }}</td>
                    <td><a class="btn btn-primary" href="{{ route('estadotable.edit',$estado) }}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    butoon
                    <form action="{{ route('estadotable.destroy',$estado->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
