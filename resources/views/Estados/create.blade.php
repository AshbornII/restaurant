@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar estado de mesa</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('estadotable.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('estadotable.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Numero de mesa:</strong>
                        <input type="float" name="numero_mesa" class="form-control"
                            placeholder="Ingresar el numero de mesa">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Estado de mesa:</strong>
                        <input type="varchar" name="estado_ocupada" class="form-control"
                            placeholder="Ingrese el estado de la mesa">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection