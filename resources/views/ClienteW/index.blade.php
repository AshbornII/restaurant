@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE CLIENTES</h1>
        <div>
            <a class="btn btn-success" href="{{ route('cliente.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre del Cliente</th>
                <th>Telefono del Cliente</th>
                <th>Direccion del Cliente </th>
                <th>Ciudad del cliente</th>
                <th>Acciones</th>
            </tr>
            @foreach ($cliente as $horario)
                <tr>
                    <td>{{ $horario->id  }}</td>
                    <td>{{ $horario->nombre_cliente }}</td>
                    <td>{{ $horario->telefono_cliente }}</td>
                    <td>{{ $horario->direccion_cliente }}</td>
                    <td>{{ $horario->ciudad_cliente }}</td>
                    <td><a class="btn btn-primary" href="{{ route('cliente.edit',$horario) }}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    <form action="{{ route('cliente.destroy',$horario->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
