@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Cliente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('cliente.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('cliente.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre del Cliente:</strong>
                        <input type="varchar" name="nombre_cliente" class="form-control"
                            placeholder="Ingrese el nombre del cliente">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Telefono del Cliente:</strong>
                        <input type="varchar" name="telefono_cliente" class="form-control"
                            placeholder="Ingrese el telefo del cliente">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Direccion del Cliente:</strong>
                        <input type="varchar" name="direccion_cliente" class="form-control"
                            placeholder="Ingrese la direccion del cliente">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Ciudad del Cliente:</strong>
                        <input type="varchar" name="ciudad_cliente" class="form-control"
                            placeholder="Ingrese la ciudadl del cliente">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection