@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar datos del Cliente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('cliente.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('cliente.update',$cliente->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre del Cliente:</strong>
                    <input type="varchar" name="nombre_cliente" class="form-control" value="{{ $cliente->dia_Atencion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Telefono del Cliente:</strong>
                    <input type="varchar" name="telefono_cliente" class="form-control" value="{{ $cliente->hora_apertura }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Direccion del Cliente:</strong>
                    <input type="varchar" name="direccion_cliente" class="form-control" value="{{ $cliente->hora_cierre }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Ciudad del Cliente:</strong>
                    <input type="varchar" name="ciudad_cliente" class="form-control" value="{{ $cliente->hora_cierre }}">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
