<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign(['menu_id_menu'], 'fk_user_has_menu_menu1')->references(['id_menu'])->on('menu')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['clientes_id_cliente'], 'fk_user_has_menu_clientes1')->references(['id_cliente'])->on('clientes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['mesas_id_mesa'], 'fk_user_has_menu_mesas1')->references(['id_mesa'])->on('mesas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign('fk_user_has_menu_menu1');
            $table->dropForeign('fk_user_has_menu_clientes1');
            $table->dropForeign('fk_user_has_menu_mesas1');
        });
    }
};
