<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaTestController;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\PedidosTable;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\MesasController;
use App\Http\Controllers\EstadodoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource("horariosprueba" , PruebaTestController::class);
Route::resource("cliente" , ClientesController::class);
Route::resource("Pedido" , PedidosTable::class);
Route::resource("menutable" , MenuController::class);
Route::resource("mesastable" , MesasController::class);
Route::resource("estadotable" , EstadodoController::class);